
import React, { Component } from 'react';
import _ from 'lodash';
import isbnIsValid from 'isbn-validator';
import Promise from 'bluebird';

import * as bookActions from '../../actions/bookActions';
import * as utilsActions from '../../actions/utilsActions';

import * as sortingFlagActions from '../../actions/sortingFlagActions';

//строка с данными о книге
class DataRow extends Component {

    constructor(props) {
		super(props);

	}

	render() {
		debugger;

		let contentBlock = '';    // данные книги
		let buttonsBlock = '';	   // доступные кнопки
		let userBlockClass = 'user-functionality-block';   
		let tableRowClass = 'table__row';

		switch (this.props.type) {

			// если строка подсвечена серым
			case 'highlight':
				tableRowClass += ' table__row-highlight';
				break;

			// если строка редактируемая
			case 'edited':
				userBlockClass += ' user-functionality-block_edited';
				tableRowClass += ' table__row-edited';

				buttonsBlock = <div className = 'user-functionality-block__buttons'>
									<button className = 'button' onClick = {this.props.saveBook}>
										Сохранить книгу
									</button>
								</div>;
				break;

			// если строка выбранная
			case 'selected':
				userBlockClass += ' user-functionality-block_selected';
				tableRowClass += ' table__row-selected';

				buttonsBlock = <div className = 'user-functionality-block__buttons'>
									<button className = 'button' onClick = {this.props.addBook}>
										Добавить книгу
									</button>
									<button className = 'button' onClick = {this.props.editBook}>
										Редактировать книгу
									</button>
									<button className = 'button' onClick = {this.props.deleteBook}>
										Удалить книгу
									</button>
								</div>;
				break;

			// если в строке нет данных о книге
			case 'noContent':
				buttonsBlock = <div className = 'user-functionality-block__buttons'>
									<button className = 'button' onClick = {this.props.addBook}>
										Добавить книгу
									</button>
								</div>;
				break;
		}

		if (this.props.content) {
			contentBlock = <div className = {tableRowClass}>
								{this.props.content}
							</div>;
		};

		return (
			<div className = {userBlockClass}>

				{contentBlock}
				{buttonsBlock}
			</div>	
		);
	}
}

// редактор книг
export default class Editor extends Component {

	constructor(props) {
		super(props);

		// заголовки таблицы
		this.columnTitles = ['Название', 'Авторы', 'Количество страниц', 'Издательство', 'Год публикации', 'Выход в тираж', 'ISBN'];

		// контейнер с данными книги по умолчанию
		this.defaultBookElement = {
			title: '',
			authors: '',
			pagesCount: '',
			publishingHouse: '',
			publishingDate: '',
			editionDate: '',
			ISBN: '',
		},

		this.state = {
			sortingFlag: 'none',   // флаг сортировки
			titleRow: {},    // строка заголовка таблицы
			dataRows: [],    // строки с данными - для каждой книги по строке
			dataRowElements: [],    // ссылки на элементы строк с данными
			selectedRow: null,     // номер выбранной строки
			editedRow: null,       // номер редактируемой строки
			data: [],              // данные о книгах
		};		

		this.fillTitleRow = this.fillTitleRow.bind(this);
		this.fillDataRows = this.fillDataRows.bind(this);
		this.selectDataRow = this.selectDataRow.bind(this);
		this.addBook = this.addBook.bind(this);
		this.editBook = this.editBook.bind(this);
		this.deleteBook = this.deleteBook.bind(this);
		this.saveBook = this.saveBook.bind(this);
		this.changeData = this.changeData.bind(this);
		this.setSorting = this.setSorting.bind(this);
		this.responseHandle = this.responseHandle.bind(this);
		this.getBooksData = this.getBooksData.bind(this);
		this.createInputElement = this.createInputElement.bind(this);
		this.checkBookData = this.checkBookData.bind(this);
	}

	// установка флага сортировки
	setSorting(event) {
		debugger;

		const newFlag = event.target.value;
		if (newFlag === this.state.sortingFlag) return;

		// кладем флаг в localStarage
		sortingFlagActions.setSortingFlag(newFlag);

		this.state.sortingFlag = newFlag;

		// запрашиваем данные с сервера
		return this.getBooksData()
			.then(response => {

				//??
			})
			.catch(error => {

				this.responseHandle(error);
			})
	}

	// изменение данных в редактируемой строке
	changeData(event) {
		debugger;

		if (this.state.editedRow === null) return;

		let editedBookData = this.state.data[this.state.editedRow];

		editedBookData[`${event.target.name}`] = event.target.value;

		this.setState({});
	}

	// добавление новой книги
	addBook() {
		debugger;
		//if (this.state.selectedRow === null) return;

		// кладем в массив новую книгу с данными по умолчанию
		const newBook = _.clone(this.defaultBookElement);
		this.state.data.push(newBook);

		// строка с этой книгой и будет редактируемой
		let editedRow = this.state.data.length - 1;

		this.setState({
			editedRow: editedRow,
			selectedRow: null,
		});
	}

	// удалить книгу
	deleteBook() {

		debugger;
		if (this.state.selectedRow === null && this.state.editedRow === null) return;

		const deletedRow = this.state.selectedRow || this.state.editedRow || 0;
		const deletedBook = this.state.data[deletedRow];

		return Promise.resolve(bookActions.deleteBook(deletedBook._id))
			.then(response => {
				
				this.state.selectedRow = null;
				this.state.editedRow = null;

				return this.getBooksData();
			})
			.then(response => {

				//??
			})
			.catch(error => {

				this.responseHandle(error);
			})
	}

	// редактировать книгу
	editBook() {

		debugger;
		if (this.state.selectedRow === null) return;

		// устанавливаем значение редактируемой строки
		this.setState({
			editedRow: this.state.selectedRow,
			selectedRow: null,  
		});
	}

	// проверка данных книги на валидность
	checkBookData(book) {
		debugger;

		let errorMessage = '';

		if (book.title == '') errorMessage = '\n введите название книги';
		else if (book.authors == '') errorMessage += '\n укажите авторов книги';
		else if (book.pagesCount == '') errorMessage += '\n укажите количество страниц';
		else if (book.pagesCount == 0 || book.pagesCount > 10000) errorMessage += '\n некорректное количество страниц';
		else if (book.publishingDate !== null && book.publishingDate !== '' && book.publishingDate < 1800) errorMessage += '\n некорректный год публикации';

		else if (book.ISBN !== '' && !isbnIsValid(book.ISBN)) errorMessage += '\n некорректный ISBN';

		// если данные невалидны, выводим сообщение об ошибке
		if (errorMessage !== '') {
			alert(`Некорректные данные книги: ${errorMessage}`);
			return false;
		}
		else return true;
	}

	// сохранить книгу
	saveBook() {
		debugger;
		
		if (this.state.editedRow === null) return;

		// сохраняемая книга
		const savedBook = this.state.data[this.state.editedRow];
		if (!savedBook) return;

		// проверяем данные книги
		let dataIsCorrect = this.checkBookData(savedBook);
		if (!dataIsCorrect) return;

		let tasks = [];

		// если у сохраняемой книги нет id, значит она не из базы, значит ее нужно сохранить
		if (savedBook._id == undefined) tasks.push(bookActions.saveBook(savedBook));
		// иначе она уже есть в базе, и ее нужно обновить
		else tasks.push(bookActions.updateBook(savedBook._id, savedBook));

		return Promise.all(tasks)
			.then(response => {

				this.state.selectedRow = this.state.editedRow;
				this.state.editedRow = null;

				return this.getBooksData();
			})
			.then(response => {

				//??
			})
			.catch(error => {

				this.responseHandle(error);
			})
	}

	// заполнить строку с заголовками таблицы
	fillTitleRow() {
		this.state.titleRow = {};

		let titleRowContent = [];
		let columnKey = 0;

		// заполняем заголовок таблицы
		this.columnTitles.forEach(title => {
			
			titleRowContent.push(<div key = {columnKey} className = 'table__column table__column-title'>{title}</div>);
			columnKey++;
		});

		this.state.titleRow = (
			<div className = 'table__row table__row-title'>
				{titleRowContent}
			</div>
		);
	}

	// получить элемент инпута для каждого типа данных (для редактирования данных)
	createInputElement(element, inputKey, value) {
		//debugger;
		let inputClass = 'input-block__field';

		let inputElement = null;

		switch (element) {

			case 'title':
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Название:</div>
									<input 
										name = "title"
										type ="text" 
										className = {inputClass} 
										maxLength = '30' 
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;							
				break;

			case 'authors':
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Авторы:</div>
									<input 
										name = "authors"
										type ="text" 
										className = {inputClass} 
										maxLength = '150' 
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;							
				break;

			case 'pagesCount':
				inputClass += ' input-block_number';
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Количество страниц:</div>
									<input 
										name = "pagesCount"
										type ="number" 
										className = {inputClass} 
										step = '1'
										min = '1'
										max = '10000'
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;
				break;

			case 'publishingHouse':
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Издательство:</div>
									<input 
										name = "publishingHouse"
										type ="text" 
										className = {inputClass} 
										maxLength = '30' 
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;	
				break;

			case 'publishingDate':
				inputClass += ' input-block_date';
				if (value === null) value = '';
				
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Год публикации:</div>
									<input 
										name = "publishingDate"
										type ="number" 
										className = {inputClass} 
										min = '1800'
										max = '2018'
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;
				break;

			case 'editionDate':
				inputClass += ' input-block_date';
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>Выход в тираж:</div>
									<input 
										name = "editionDate"
										type ="date" 
										className = {inputClass} 
										min="1800-01-01" 
										max="2018-12-31"
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;	
				break;

			case 'ISBN':
				inputElement = <div key = {inputKey} className = 'input-block'>
									<div className = 'input-block__name'>ISBN:</div>
									<input 
										name = "ISBN"
										type ="text" 
										className = {inputClass} 
										maxLength = '150' 
										value = {value}
										onChange = {this.changeData}
									/>
								</div>;
				break;
		}

		return inputElement;
	}

	// заполнить строки с данными о книгах
	fillDataRows() {

		this.state.dataRows = [];  // строки
		let dataRowElements = [];  // ссылки на элементы строк
		let dataRowKey = 1;  // уникальный ключ для каждой строки

		// если нет книг в массиве, то показываем только кнопку "добавить"
		if (!this.state.data.length) {

			let dataRow = <DataRow 
								type = 'noContent' 
								key = {dataRowKey}
								addBook = {this.addBook}
								editBook = {this.editBook}
								deleteBook = {this.deleteBook}
								saveBook = {this.saveBook}
							/>;

			this.state.dataRows.push(dataRow);
		}

		// если есть книги, то заполняем строки с данными
		this.state.data.forEach((bookData) => {

			debugger;
			let dataRowType = '';  // тип строки - редактируемая/выбранная/обычная
			let dataRowContent = [];   // данные в строке

			// если это редактируемая книга
			if (this.state.editedRow !== null && this.state.data.indexOf(bookData) == this.state.editedRow) {
				
				let inputKey = 0;  // уникальный ключ для каждого инпута

				// заполняем данные строки - т.к. их надо редактировать, то данные кладем в инпуты
				for (let element in bookData) {

					//if (element == '_id') continue;
					let inputElement = this.createInputElement(element, inputKey, bookData[element]);	
					inputKey++;

					if (inputElement) dataRowContent.push(inputElement);
				};

				dataRowType = 'edited';
			}
			// если строка не редактируемая
			else {

				let columnKey = 0;  // ключ для блока с данными

				// заполняем данные строки
				for (let element in bookData) {

					if (element == '_id') continue;
					dataRowContent.push(<div key = {columnKey} className = 'table__column'>{bookData[element]}</div>);
					columnKey++;
				};

				// если строка выбранная
				if (this.state.selectedRow !== null && this.state.data.indexOf(bookData) == this.state.selectedRow) {
					
					dataRowType = 'selected';
				}
				// если строка не редактируемая и не выбранная
				else {
					// строки через одну подсвечиваются серым - для читабельности
					dataRowType = (dataRowKey % 2 == 0) ? 'highlight' : 'default';  
				}
			}		
			
			let dataRow = <DataRow 
								type = {dataRowType}  
								key = {dataRowKey}
								content = {dataRowContent}
								addBook = {this.addBook}
								editBook = {this.editBook}
								deleteBook = {this.deleteBook}
								saveBook = {this.saveBook}
							/>;

			this.state.dataRows.push(
				<div ref = {elem => dataRowElements.push(elem)} key = {dataRowKey}>
					{dataRow}
				</div>
			);

			dataRowKey++;
		});

		this.state.dataRowElements = dataRowElements; //??
	}

	// выбрать книгу
	selectDataRow(event) {
		debugger;
		if (!event.currentTarget) return;  //??

		let rowIndex = null;

		// получаем номер выбранной строки
		this.state.dataRowElements.forEach((elem) => {
			if (elem == event.currentTarget) {
				rowIndex = this.state.dataRowElements.indexOf(elem);
			}
		})
		
		this.setState({
			selectedRow: rowIndex,
        });
	}

	// обработчик сообщений для пользователя
	responseHandle(response) {
		debugger;

		if (response.response) response = response.response;  // если это ошибка

		const message = utilsActions.getResponseMessage(response);
		alert(message);
	}

	// получить данные с книгами с сервера
	getBooksData() {
		debugger;

		return bookActions.getAllBooks(this.state.sortingFlag)
			.then(response => {
				debugger;

				this.setState({
					data: response.data,
				});

				return true;
			})
			.catch(error => {

				this.responseHandle(error);
			})
	}

	componentWillMount() {
		debugger;

		// заполняем заголовок
		this.fillTitleRow();

		// получаем флаг сортировки из хранилища, если он там есть
		const sortingFlag = sortingFlagActions.getSortingFlag();
		if (sortingFlag !== null) {
			this.state.sortingFlag = sortingFlag;
		}

		return this.getBooksData(); //??
			/*.then(response => {
				//??
			})
			.catch(error => {
				
				this.responseHandle(error);
			})*/
	}

	componentDidMount() {
		debugger;

		// вешаем событие выбора на каждую строку
		this.state.dataRowElements.forEach((elem) => {
			elem.addEventListener('click', this.selectDataRow);
		})
	}

	componentWillUpdate() {
		debugger;

		// удаляем обработчик у каждой строки
		this.state.dataRowElements.forEach((elem) => {
			elem.removeEventListener('click', this.selectDataRow);
		})
	}

	componentDidUpdate() {
		debugger;

		// если какая-либо книга редактируется, то нельзя выбрать другую книгу
		if (this.state.editedRow === null) {
			// вешаем событие выбора на каждую строку
			this.state.dataRowElements.forEach((elem) => {
				elem.addEventListener('click', this.selectDataRow);
			});
		}
	}

	render() {

		const editorClass = 'editor ' + (this.props.className ? this.props.className : '');

		let sortIsDisabled;  // флаг, доступна ли сортировка
		let sortTitle;
		let selectClass = 'sorting-block__select';

		if (this.state.editedRow !== null) {
			sortIsDisabled = true;
			sortTitle = 'Во время редактирования данных книги сортировка недоступна';
			selectClass += ' sorting-block_disabled';
		}
		else {
			sortIsDisabled = false;
			sortTitle = '';
			selectClass += ' sorting-block_enabled';
		}

		this.fillDataRows();
        
        return (
            <div className = {editorClass}>

				<div className = 'sorting-block'>
					<div className = 'sorting-block__name'>Сортировать по: </div>
					<select 
						name="sorting" 
						title = {sortTitle}
						disabled = {sortIsDisabled} 
						className = {selectClass} 
						onChange = {this.setSorting} 
						value = {this.state.sortingFlag}>
							<option value="none">-</option>
							<option value="title">названию</option>
							<option value="publishingDate">году публикации</option>
					</select>
				</div>

				<div className = 'table'>

					{this.state.titleRow}
					
					{this.state.dataRows}

				</div>
                
            </div>
        )
	}
}