
import React, { Component } from 'react';

export default class Footer extends Component {

    render() {

        const footerClass = 'footer ' + (this.props.className ? this.props.className : '');
        
        return (
            <div className = {footerClass}>

                 <div className ="footer__title">
                    &#169; BookEditorCompany, 2018
                </div>
            </div>
        )
    }
}