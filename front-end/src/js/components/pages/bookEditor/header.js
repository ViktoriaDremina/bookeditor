
import React, { Component } from 'react';

export default class Header extends Component {

    render() {

		const headerClass = 'header ' + (this.props.className ? this.props.className : '');

        return (
			<div className = {headerClass}>
				<div className = 'header__title'>
					Редактор книг
				</div>
			</div>
        )
    }
}
