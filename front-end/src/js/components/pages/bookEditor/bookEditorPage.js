
import React, { Component } from 'react';

import Header from './header';
import Footer from './footer';
import Editor from './editor';

// страница редактора книг
export default class BookEditorPage extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		return (

			<div className = 'page'>
				<Header className = 'page__header'/>
			
				<Editor/>

				<Footer className = 'page__footer'/>
			</div>
		)
	}
}