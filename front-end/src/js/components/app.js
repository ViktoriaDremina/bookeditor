
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import BookEditorPage from './pages/bookEditor/bookEditorPage';
import NotFoundPage from './pages/notFound/notFoundPage';

export default class App extends Component {

    render() {

        return (

            <Switch>

                <Route exact path="/" component={BookEditorPage} />     

                <Route component={NotFoundPage} />
                
            </Switch>
        )
    }
}