
// помещение и получения флага сортировки из localStorage

export function getSortingFlag() {

	return localStorage.getItem('sortingFlag');
};

export function setSortingFlag(sortingFlag) {

	localStorage.setItem('sortingFlag', sortingFlag);
};
    
export function removeSortingFlag() {

	localStorage.removeItem("sortingFlag");
};
  
  