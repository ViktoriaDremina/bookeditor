
import axios from 'axios';
const Promise = require('bluebird');

import apiConst from '../apiConst';

// CRUD-запросы к серверу:

export function getAllBooks(sortingFlag) {
	debugger;

	let uri = apiConst.books;

	// если есть сортировка, то добавляем этот параметр в запрос
	switch (sortingFlag) {

		case 'title':
			uri += '?sort=title';
			break;
		
		case 'publishingDate':
			uri += '?sort=publishingdate';
			break;
	}

	return axios.get(`${uri}`);
};

export function saveBook(data) {
	debugger;

	return axios.post(`${apiConst.books}`, data);
};

export function updateBook(id, data) {
	debugger;

	return axios.put(`${apiConst.books + id}`, data);
};

export function deleteBook(id) {
	debugger;

	return axios.delete(`${apiConst.books + id}`);
};