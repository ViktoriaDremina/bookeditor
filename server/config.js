
//const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
    version: '1.0'
    , server: {
        port: process.env.PORT || 3000
        , host: 'localhost'
        , protocol: 'http'
    }
    , db : {
        mongo : {
            url: 'mongodb://me:me123456@ds048279.mlab.com:48279/books'  
            , options: {
                autoReconnect: (process.env.NODE_ENV == 'production')  
                , useNewUrlParser: true 
            }
        }
    }
};