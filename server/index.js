
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const config = require('./config');
const mongoDbUtils = require('./api/lib/mongoDbUtils');
const utils = require('./api/lib/utils');

const indexHTML = path.resolve('./front-end/public/index.html');
const app = express();

// статические файлы
app.use(express.static('front-end/public'));

app.use((req, res, next) => {
    res.set({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'
    });

    next();
});

app.use(bodyParser.json({type: 'application/json'}));

//соединение с БД
mongoDbUtils.setUpConnection();  

// ---------------------------------------------------------------
// запросы к api
app.use('/api', require('./api/routes/book'));

// ---------------------------------------------------------------

// на все остальные запросы отдаем главную страницу
app.get('/*', (req, res) => res.sendFile(indexHTML));

// Если произошла ошибка валидации, то отдаем 400 Bad Request
app.use((req, res, next) => {

    const errorMessage = `url does not exist: ${req.url}`;

    return utils.sendErrorResponse(res, errorMessage, 404);
});

// Если же произошла иная ошибка, то отдаем 500 Internal Server Error
app.use((err, req, res, next) => {

    const errorMessage = err.message ? err.message : '';

    return utils.sendErrorResponse(res, errorMessage, 500);
});

app.listen(config.server.port, () => {  
    console.log(`Hosted on:  ${config.server.host}:${config.server.port}`);
});
