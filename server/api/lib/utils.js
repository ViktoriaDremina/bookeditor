
const Promise = require('bluebird');

const config = require('../../config');

const utils = new function() {
 
    this.sendResponse = function(res, response, statusCode) {

        let status = statusCode || 200;
        let responseData = response ? response : 'OK'; //??

        return res.status(status).send(responseData);
    };

	this.sendErrorResponse = function(res, error, statusCode) {

        let status = statusCode || error.statusCode || 500;
        if (error == 'UNSUPPORTED_METHOD') status = 404; //???

        let errorMessage = error.message || error || 'error'; //??

        return res.status(status).send(errorMessage);
    };

};

module.exports = utils;