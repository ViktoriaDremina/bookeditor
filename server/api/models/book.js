const mongoose = require('mongoose');

const bookSchema = require('../schemas/book');

const BookModel = mongoose.model('Book', bookSchema);

module.exports = {
	
	query: function(config) {

		// если без сортировки
		if (!config.sort) return BookModel.find(config);

		// с сортировкой
		return Promise.resolve(   // обертка для того, чтобы запрос возращал промис
			BookModel
			.find()
			.sort(`${config.sort}`)
			.exec())
			  .then((result) => {

				return result;
			  })
	},
	
	create: function(data) {
		const book = new BookModel({
			title     : data.title,
			authors     : data.authors,
			pagesCount: data.pagesCount,
			publishingHouse : data.publishingHouse,
			publishingDate : data.publishingDate,
			editionDate: data.editionDate,
			ISBN     : data.ISBN,
		});
	
		return book.save();
	},

	update: function(id, data) {
		const book = new BookModel({
			_id: id,
			title     : data.title,
			authors     : data.authors,
			pagesCount: data.pagesCount,
			publishingHouse : data.publishingHouse,
			publishingDate : data.publishingDate,
			editionDate: data.editionDate,
			ISBN     : data.ISBN,
		});

		return BookModel.findOneAndUpdate({_id: id}, book, {new: true});
	},
	
	delete: function(id) {
		return BookModel.findOneAndRemove({_id: id});
	},
}