
const mongoose = require('mongoose');
//const filePluginLib = require('mongoose-file');
//const path = require('path');

const Schema = mongoose.Schema;

const bookSchema = new Schema(
	{
		title     : { type: String },
		authors     : { type: String },
		pagesCount: { type: Number },
		publishingHouse : {type: String },
		publishingDate     : { type: Number },
		editionDate: { type: String },
		ISBN     : { type: String },
	},
	{versionKey: false}   //отключение поля __v, которое указывает на версию документа
);

/*
const filePlugin = filePluginLib.filePlugin;
const make_upload_to_model = filePluginLib.make_upload_to_model;

var uploads_base = path.join(__dirname, './server/webdata');
var uploads = path.join(uploads_base, "upload");

bookSchema.plugin(filePlugin, {
    name: "picture",
    upload_to: make_upload_to_model(uploads, 'pictures'),
    relative_to: uploads_base
});
*/

module.exports = bookSchema;

