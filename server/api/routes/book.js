
const express = require('express');
const Promise = require('bluebird');

const bookModel = require('../models/book');
const utils = require('../lib/utils');

let router = express.Router();

//----- endpoint: /api/book/
router.route('/book')

	// получение всех книг
  .get(function(req, res) { 

		// если есть параметр сортировки в запросе, то кладем его в конфиг
		let config = (req.query.sort) ? req.query : {};  

    return bookModel.query(config)
      .then((data) => {

        return utils.sendResponse(res, data);
      })
      .catch((error) => {

				return utils.sendErrorResponse(res, error, 500);
			});
  })

	// сохранение книги
  .post(function(req, res) {
    
    return Promise.resolve(true)
			.then(() => {

				if (!req.body) throw new Error('no book data in req');

				return bookModel.create(req.body);
			})
			.then((dbResponse) => {

				if (dbResponse.errors) {

					// log errors
					dbResponse.errors.forEach((error) => {
						console.log('book saved with error: ' + error.message);
					});

					throw new Error('book saved with error');
				}

				return utils.sendResponse(res, 'book saved successfully', 201);
			})
			.catch((error) => {

				return utils.sendErrorResponse(res, error, 500);
			});
  })
  
  .put(function(req, res) {

		return utils.sendErrorResponse(res, 'UNSUPPORTED_METHOD');
  })
  
  .delete(function(req, res) {

		return utils.sendErrorResponse(res, 'UNSUPPORTED_METHOD');
	})
;

//----- endpoint: /api/book/:id
router.route('/book/:id')

  // получение книги по ее id
  .get(function(req, res) {   
    
    return bookModel.query({_id: req.params.id})
      .then((data) => {

        return utils.sendResponse(res, data);
      })
      .catch((error) => {

				return utils.sendErrorResponse(res, error, 500);
			});
  })

  .post(function(req, res) {

		return utils.sendErrorResponse(res, 'UNSUPPORTED_METHOD');
	})

  // редактирование данных книги по ее id
  .put(function(req, res) {

    return Promise.resolve(true)
			.then(() => {

				if (!req.body) throw new Error('no book data in req');

				return bookModel.update(req.params.id, req.body);
			})
			.then((dbResponse) => {

				if (dbResponse.errors) {

					// log errors
					dbResponse.errors.forEach((error) => {
						console.log('book saved with error: ' + error.message);
					});

					throw new Error('book saved with error');
				}

				return utils.sendResponse(res, 'book saved successfully', 201);
			})
			.catch((error) => {

				return utils.sendErrorResponse(res, error, 500);
			});
  })

  .delete(function(req, res) {

    return bookModel.delete({_id: req.params.id})
    .then((dbResponse) => {

      if (dbResponse.errors) {

        // log errors
        dbResponse.errors.forEach((error) => {
          console.log('book deleted with error: ' + error.message);
        });

        throw new Error('book deleted with error');
      }

      return utils.sendResponse(res, 'book deleted successfully', 204);
    })
    .catch((error) => {

      return utils.sendErrorResponse(res, error, 500);
    });
  })
;

module.exports = router;
